import { Component, Input, ElementRef, AfterViewInit, ViewChild} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Shape} from './models/shape';
import {Line} from './models/line';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  context:CanvasRenderingContext2D;
title = 'app';
sh:Shape;
width:any;
height:any;

valid = false;
public dragging = false;
selection:any;
dragoffx:any;
dragoffy:any;
shapes:any[];
lines:any[];
  @ViewChild("myCanvas") public canvas: ElementRef;
    public ngAfterViewInit() {
     const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.context = canvasEl.getContext('2d');
 this.context.lineWidth = 3;
    

    this.width = canvasEl.width;
    this.height = canvasEl.height;
    this.valid = false; 
    this.shapes = [];  
    this.lines = [];
    this.dragging = false; 
    this.selection = null;
    this.dragoffx = 0; 
    this.dragoffy = 0;
canvasEl.addEventListener('selectstart',(x)=>{x.preventDefault();return false;},false);
canvasEl.addEventListener('mousedown',(x)=>{
 
  let mx = x.x;
    let my = x.y;
  //    console.log("mouse down");
  //     console.log("x="+mx);
  //      console.log("y="+my);
  //      this.sh = new Shape(mx,my,100,100,'#AAAAAA');
  // this.sh.draw(this.context);
  let l = this.shapes.length;
 
      for (let i = l-1; i >= 0; i--) {
      if (this.shapes[i].contains(mx, my)) {
        let mySel =this.shapes[i];

    
        if(this.selection){
          this.addLine(this.selection.order,mySel.order);
       this.drawLine(this.lines[this.lines.length-1]);
        }
        this.dragoffx = mx - mySel.x;
        this.dragoffy = my - mySel.y;
        this.dragging = true;
        this.selection = mySel;
        this.valid = false;  
        console.log(this.shapes);
        console.log(this.selection);
        return;
      }
   
    }
       if (this.selection) {
      this.selection = null;
      this.valid = false; 
    }
},true);

      canvasEl.addEventListener('mousemove', (x)=> {
    if (this.dragging){
    
 
      this.selection.x = x.x - this.dragoffx;
      this.selection.y = x.y -this.dragoffy;   
      this.valid = false; 
      this.draw();
    }
  }, true);
      canvasEl.addEventListener('mouseup', (x)=> {
    this.dragging = false;
       for(let i = 0 ; i< this.lines.length;i++){
      let temp:Line = this.lines[i];
       this.drawLine(temp);
      //console.log(temp);
    }
  }, true);
    canvasEl.addEventListener('dblclick', (x) =>{
    let temp = new Shape(x.x - 10, x.y - 10, 100, 70, 'rgba(0,255,0,.6)')
    temp.draw(this.context);
    this.shapes.push(temp);
    this.shapes[this.shapes.length -1].order = this.shapes.length -1;
  }, true);
  }



draw () {
 
  if (!this.valid) {
    var ctx = this.context;
    var shapes = this.shapes;
this.context.clearRect(0, 0, 800, 600);
    console.log("clearing");
  
    var l = shapes.length;
    for (let i = 0; i < l; i++) {
      let shape = shapes[i];
      
      if (shape.x > this.width || shape.y > this.height ||
          shape.x + shape.w < 0 || shape.y + shape.h < 0) continue;
      shapes[i].draw(ctx);
    }
   
    
    if (this.selection != null) {
      ctx.strokeStyle = "#fff";
      ctx.lineWidth = 1;
      var mySel = this.selection;
      ctx.strokeRect(mySel.x,mySel.y,mySel.w,mySel.h);
    }
    
  
    this.valid = true;
  }
}
drawLine(line:Line){
 
   let shapeA:Shape = this.shapes[line.shapeA];
  let shapeB:Shape = this.shapes[line.shapeB];
 
     this.context.beginPath();
        this.context.moveTo(shapeA.x+shapeA.w,shapeA.y+(shapeA.h/2));
         this.context.lineTo(shapeB.x, shapeB.y+(shapeB.h/2));
          this.context.lineJoin = 'round';
         this.context.stroke();
       
}


addLine(a,b){
   let shapeA:Shape = this.shapes[a];
  let shapeB:Shape = this.shapes[b];
  if(shapeA.order == shapeB.order)
  return
   if(shapeA.x >shapeB.x){
    let temp = shapeA;
    shapeA = shapeB;
    shapeB = temp;
  }
  for(let i=0;i<this.lines.length;i++ ){
    let element:Line = this.lines[i];
    if(shapeA.order == element.shapeA && shapeB.order == element.shapeB)
    return;
    if(shapeA.order == element.shapeB && shapeB.order == element.shapeA)
    return;
  }
  console.log("line added");
 this.lines.push(new Line(shapeA.order,shapeB.order)); 
}


}
