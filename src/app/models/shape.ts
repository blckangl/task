 export class Shape{
order:number;
x:any;
y:any;
w:any;
h:any;
fill:any;
    constructor(x, y, w, h, fill){
            this.x = x || 0;
            this.y = y || 0;
           this.w = w || 1;
            this.h = h || 1;
            this.fill = fill || '#AAAAAA';
    }
 public draw (ctx){
  ctx.fillStyle = this.fill;
  ctx.fillRect(this.x, this.y, this.w, this.h);
}
public contains(mx, my):boolean{
    console.log((this.x <= mx) && (this.x + this.w >= mx) &&
          (this.y <= my) && (this.y + this.h >= my));
    return  (this.x <= mx) && (this.x + this.w >= mx) &&
          (this.y <= my) && (this.y + this.h >= my);
}
}